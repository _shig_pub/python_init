#!/bin/bash

# python env name
env_name="env"
# python bin name
bin_python="python3"

echo ""
echo "-------------"
echo "Creating python venv and init git"
echo "-------------"
echo ""
read -p "Folder name : " folder

if [ -d "$(pwd)/${folder}" ]; then
	echo "'$(pwd)/${folder}' already exist"
	read -p "Do you want the script to delete it before continuing (y/n) ? " answer
	answer=${answer^^}
	if [ "${answer}" == "Y" ]; then
		echo "Deleting '$(pwd)/${folder}'..."
		rm -Rf "$(pwd)/${folder}"
		echo "Re-creating $(pwd)/${folder}..."
		mkdir -p "$(pwd)/${folder}"
	else
		echo "Exiting !"
		echo ""
		exit 0
	fi
else
	echo "Creating $(pwd)/${folder}..."
	mkdir -p "$(pwd)/${folder}"
fi
echo "Going to $(pwd)/${folder}..."
cd "$(pwd)/${folder}"
echo "Creating python venv '${env_name}'..."
${bin_python} -m venv env
echo "---------"
echo "How to activate venv : type '\$ source env/bin/activate' being in $(pwd)/${folder}"
echo "How to deactivate venv : type '\$ deactivate' being in $(pwd)/${folder}"
echo "---------"
echo "Activating git..." 
git  init 2>/dev/null 1>&2
touch .gitignore
echo env/ > .gitignore
echo "Done !"
echo ""
